/**
 * SFP (SQL file parser)
 * Takes the 'sql.txt' file in the root repository,
 * cleans it and parse the data into a CSV file
 * named 'data.txt'
 * 
 * To use this project, replace the 'sql.txt' with
 * the project needed and execute this line in
 * a terminal : 
 * node --max-old-space-size=8142 splitSQLintoJSON.js
 */
const fs = require('fs');

var csvData = "";
var start = 0, end = 100;

fs.readFile('sql.txt', 'utf8', function (err, data) {
    if(err) console.log('error', err);
    else extractDataLines(data);
});

//From the SQL file in txt format, takes only the part were the data is inserted
function extractDataLines(rawData) {
    inputData = dataUtf8Cleanup(rawData);
    index = inputData.search("'[A-Z][0-9][A-Z][0-9][A-Z][0-9]'")-1;
    extractAddresses(inputData.slice(index));
}

//Remove all uncaught special character and replace them
function dataUtf8Cleanup(dirtyData) {
    cleanupData = dirtyData.split("\\u00c0").join("À");
    cleanupData = cleanupData.split("\\u00c2").join("Â");
    cleanupData = cleanupData.split("\\u00c7").join("Ç");
    cleanupData = cleanupData.split("\\u00c8").join("È");
    cleanupData = cleanupData.split("\\u00c9").join("É");
    cleanupData = cleanupData.split("\\u00ca").join("Ê");
    cleanupData = cleanupData.split("\\u00cb").join("Ë");
    cleanupData = cleanupData.split("\\u00ce").join("Î");
    cleanupData = cleanupData.split("\\u00cf").join("Ï");
    cleanupData = cleanupData.split("\\u00d4").join("Ô");
    cleanupData = cleanupData.split("\\u00db").join("Û");
    cleanupData = cleanupData.split("\\").join("");
    return cleanupData;
}

//From the string with all addresses, split the values when there is a "\n"
function extractAddresses(inputDataLines) {
    splits = inputDataLines.split("\n")
    for(i = 0; i < splits.length; i++) {
        if(!splits[i].startsWith("INSERT")) {
            findUsefullInfo(splits[i]);
        }
    }
}

//From each value in the table, takes only the ones with an address associated to the postal code
function findUsefullInfo(extractedAddresses) {
    addresses = extractedAddresses.split(", ", 3);
    if(addresses[1] == '1' && !addresses[2].startsWith("''")) {
        addressesJsonFile = JSON.parse(addresses[2].slice(1, -3));
        postalCodeCSVParsing(addresses[0].slice(2, -1), addressesJsonFile);
    }
}

//From the extracted data, parse all items and creates a CSV file
function postalCodeCSVParsing(pc, ajf) {
    delete ajf.No_Franchise;
    delete ajf.NO_FRANCHISE_EXT;
    delete Object.values(ajf.VILLES)[0].Id_Ville;
    city = Object.keys(ajf.VILLES);
    streetTypes = Object.values(ajf.VILLES)[0].Street;
    for(streetType in streetTypes) {
        streetNames = streetTypes[streetType];
        for(streetName in streetNames) {
            streetNumbers = streetNames[streetName].NoCIVIC;
            for(streetNumber in streetNumbers) {
                area = streetNumbers[streetNumber].area;
                csvData += pc + " , " + city + " , " + streetType + " , " + streetName + " , " + streetNumber + " , " + area + "\n";
            };
        };
        writeCSVdataToFile(csvData);
    };
}

function writeCSVdataToFile(data) {
    fs.appendFileSync('data.txt', data, function(err) {
        if (err) {
            console.log(err);
        }
    });
    csvData = '';
}